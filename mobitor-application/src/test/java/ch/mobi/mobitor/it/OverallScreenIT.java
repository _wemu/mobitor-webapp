package ch.mobi.mobitor.it;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentPipeline;
import ch.mobi.mobitor.domain.configgenerator.generator.OverallScreenConfigGenerator;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.TestEnvConfigProps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

public class OverallScreenIT extends AbstractScreenIT {

    private static final String SCREEN_KEY = "O";

    @MockBean
    private EnvironmentsConfigurationService environmentsConfigurationService;

    @BeforeEach
    public void setup() {
        when(environmentsConfigurationService.getEnvironmentConfig(any())).thenReturn(new TestEnvConfigProps("testenv"));
        when(environmentsConfigurationService.getPipeline(any())).thenReturn(EnvironmentPipeline.ANY);
    }

    @Autowired
    private OverallScreenConfigGenerator generator;

    @Autowired
    private ServersConfigurationService serversConfigurationService;

    public void printHtml() throws Exception {
        performGetScreen(SCREEN_KEY).andDo(print());
    }

    @Test
    public void renderedScreenShouldHaveCorrectTitle() throws Exception {
        //given
        Screen screen = findScreen(SCREEN_KEY);

        //when
        ResultActions resultActions = performGetScreen(screen.getConfigKey());

        //then
        resultActions.andExpect(screenTitleContains(screen.getLabel()));
    }

    @Test
    public void screenShouldContainAllConfiguredEnvironments() {
        //given
        Screen screen = findScreen(SCREEN_KEY);

        //when
        List<String> environments = screen.getEnvironments();

        //then
        assertThat(environments).containsAll(generator.getEnvironments());
    }

    @Test
    public void renderedScreenShouldContainAllConfiguredEnvironments() throws Exception {
        //when
        ResultActions resultActions = performGetScreen(SCREEN_KEY);

        //then
        for (String environment : generator.getEnvironments()) {
            resultActions.andExpect(screenContainsEnvironmentColumn(environment));
        }
    }

    @Test
    public void renderedScreenShouldContainCorrectNumberOfEnvironmentColumns() throws Exception {
        //when
        ResultActions resultActions = performGetScreen(SCREEN_KEY);

        //then
        resultActions.andExpect(screenContainsNumberOfEnvironmentColumns(generator.getEnvironments().size()));
    }

}
