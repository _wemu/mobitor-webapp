package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.domain.config.ChangelogConfig;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.TestEnvConfigProps;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChangelogConfigurationServiceTest {

    @Test
    public void getAllChangelogConfigs() {
        // arrange
        ChangelogConfigurationService changelogConfigurationService = new ChangelogConfigurationService(new DefaultResourceLoader());
        changelogConfigurationService.initializeChangelogRepositories();

        // act
        List<ChangelogConfig> allChangelogConfigs = changelogConfigurationService.getAllChangelogConfigs();

        // assert
        assertThat(allChangelogConfigs).isNotNull();
        assertThat(allChangelogConfigs).isNotEmpty();
    }

    @Test
    public void getAllChangelogConfigsHaveKnownAmwServer() {
        // arrange
        ServersConfigurationService serversConfigurationService = new ServersConfigurationService(new DefaultResourceLoader());
        serversConfigurationService.initializeAmwDeployments();

        ChangelogConfigurationService changelogConfigurationService = new ChangelogConfigurationService(new DefaultResourceLoader());
        changelogConfigurationService.initializeChangelogRepositories();

        // act
        Map<String, AppServerConfig> appServerNameToConfigMap = serversConfigurationService.getAppServerNameToConfigMap();
        List<ChangelogConfig> allChangelogConfigs = changelogConfigurationService.getAllChangelogConfigs();

        // assert
        for (ChangelogConfig changelogConfig : allChangelogConfigs) {
            String serverName = changelogConfig.getServerName();
            String amwApplicationName = changelogConfig.getApplicationName();

            AppServerConfig appServerConfig = appServerNameToConfigMap.get(serverName);

            // changelog uses known Server
            assertThat(appServerNameToConfigMap.keySet()).contains(serverName);
            // changelog uses known Application
            assertThat(appServerConfig.getApplicationNames()).contains(amwApplicationName);
        }
    }

    @Test
    public void getAllChangelogConfigsHaveKnownEnvironments() {
        // arrange
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        List<EnvironmentConfigProperties> envConfPropList = new ArrayList<>();
        envConfPropList.add(new TestEnvConfigProps("preprod"));
        envConfPropList.add(new TestEnvConfigProps("prod"));
        when(environmentsConfigurationService.getEnvironments()).thenReturn(envConfPropList);

        List<String> allEnvironments = environmentsConfigurationService.getEnvironments().stream().map(EnvironmentConfigProperties::getEnvironment).collect(Collectors.toList());

        ChangelogConfigurationService changelogConfigurationService = new ChangelogConfigurationService(new DefaultResourceLoader());
        changelogConfigurationService.initializeChangelogRepositories();

        // act
        List<ChangelogConfig> allChangelogConfigs = changelogConfigurationService.getAllChangelogConfigs();

        // assert
        for (ChangelogConfig changelogConfig : allChangelogConfigs) {
            List<String> environments = changelogConfig.getEnvironments();

            environments.forEach(env -> {
                assertThat(env).isNotEmpty();
                assertThat(allEnvironments).contains(env);
            });
        }
    }

}
