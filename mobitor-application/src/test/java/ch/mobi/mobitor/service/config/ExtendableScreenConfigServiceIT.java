package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("allpluginsenabledtest")
@ComponentScan(basePackages = {"ch.mobi.mobitor"}, excludeFilters = @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class))
@DirtiesContext
public class ExtendableScreenConfigServiceIT {

    @Autowired
    private MobitorPluginsRegistry pluginsRegistry;

    @Autowired
    private ExtendableScreenConfigService configService;

    @Test
    public void testConfigDeserializer() {
        List<ExtendableScreenConfig> configs = configService.getConfigs();
        assertThat(configs).isNotNull();

        configs.forEach(config -> {
            System.out.println("Screen-Config-Key: " + config.getConfigKey());

            assertThat(config.getConfigKey()).isNotNull();
            assertThat(config.getLabel()).isNotNull();
            assertThat(config.getEnvironments()).isNotNull();
            assertThat(config.getServerNames()).isNotNull();

            Map<String, List> pluginConfigMap = config.getPluginConfigMap();
            assertThat(pluginConfigMap).isNotNull();
            assertThat(pluginConfigMap.size()).isGreaterThan(0);

            pluginConfigMap.forEach((configPropName, configList) -> {
                assertThat(configPropName).isNotNull();
                assertThat(configList).isNotNull();

                MobitorPlugin mobitorPlugin = pluginsRegistry.getPlugin(configPropName);
                Class configClass = mobitorPlugin.getConfigClass();
                configList.forEach(configItem -> assertThat(configItem.getClass().isAssignableFrom(configClass)).isTrue());
            });

            // Every configuration in a screen has a Plugin, no dead or not parsed config
            assertThat( config.getUnreadPluginConfigProperties()).isEmpty();
        });
    }

}
