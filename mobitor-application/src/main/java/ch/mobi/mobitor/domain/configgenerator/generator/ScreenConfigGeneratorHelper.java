package ch.mobi.mobitor.domain.configgenerator.generator;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import ch.mobi.mobitor.plugin.sonarqube.config.SonarProjectConfig;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityBuildConfig;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityProjectConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

class ScreenConfigGeneratorHelper {

    static void addBuildConfigsWithServerAssociation(final List<TeamCityBuildConfig> buildConfigs, LinkedHashMap<String, TeamCityBuildConfig> builds) {
        // only add builds associated to a server and application to the overall screen:
        buildConfigs.stream()
                .filter(buildConfig -> StringUtils.isNoneBlank(buildConfig.getServerName(), buildConfig.getApplicationName()))
                .forEach(buildConfig -> builds.put(buildConfig.getConfigId(), buildConfig));
    }

    static void addProjectConfigsWithServerAssociation(final List<TeamCityProjectConfig> projectConfigs, LinkedHashMap<String, TeamCityProjectConfig> projects) {
        // only add builds associated to a server and application to the overall screen:
        projectConfigs.stream()
                .filter(projectConfig -> StringUtils.isNoneBlank(projectConfig.getServerName(), projectConfig.getApplicationName()))
                .forEach(projectConfig -> projects.put(projectConfig.getProjectId(), projectConfig));
    }

    static void addSonarConfigsWithServerAssociation(final List<SonarProjectConfig> sonarConfigs, LinkedHashMap<String, SonarProjectConfig> sonars) {
        sonarConfigs.stream()
                .filter(sonarConfig -> StringUtils.isNoneBlank(sonarConfig.getServerName(), sonarConfig.getApplicationName()))
                .forEach(sonarConfig -> sonars.put(sonarConfig.getProjectKey(), sonarConfig));
    }

    static void addAllRestServiceConfigs(List<RestServiceConfig> restServiceConfigs, LinkedHashMap<String, RestServiceConfig> restServices) {
        for (RestServiceConfig restServiceConfig : restServiceConfigs) {
            // if swaggerUri is empty, use first additionalUri - and pray?
            if (StringUtils.isNotEmpty(restServiceConfig.getSwaggerUri())) {
                restServices.put(restServiceConfig.getSwaggerUri(), restServiceConfig);
            } else {
                restServices.put(restServiceConfig.getAdditionalUris().get(0).getUri(), restServiceConfig);
            }
        }
    }

}
